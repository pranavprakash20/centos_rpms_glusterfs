From cb681aeb67fbca52e8e0aab04a909e4bf8a62174 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?G=C3=BCnther=20Deschner?= <gd@samba.org>
Date: Mon, 9 Oct 2017 18:02:25 +0200
Subject: [PATCH 130/139] md-cache: avoid checking the xattr value buffer with
 string functions.
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

xattrs may very well contain binary, non-text data with leading 0
values. Using strcmp for checking empty values is not the appropriate
thing to do: In the best case, it might treat a binary xattr value
starting with 0 from being cached (and hence also from being reported
back with xattr). In the worst case, we might read beyond the end
of a data blob that does contain any zero byte.

We fix this by checking the length of the data blob and checking
the first byte against 0 if the length is one.

> Signed-off-by: Guenther Deschner <gd@samba.org>
> Pair-Programmed-With: Michael Adam <obnox@samba.org>
> Change-Id: If723c465a630b8a37b6be58782a2724df7ac6b11
> BUG: 1476324
> Reviewed-on: https://review.gluster.org/17910
> Reviewed-by: Michael Adam <obnox@samba.org>
> Smoke: Gluster Build System <jenkins@build.gluster.org>
> Reviewed-by: Poornima G <pgurusid@redhat.com>
> Tested-by: Poornima G <pgurusid@redhat.com>
> CentOS-regression: Gluster Build System <jenkins@build.gluster.org>
> (cherry picked from commit ab4ffdac9dec1867f2d9b33242179cf2b347319d)

Change-Id: If723c465a630b8a37b6be58782a2724df7ac6b11
BUG: 1446125
Signed-off-by: Günther Deschner <gd@samba.org>
Reviewed-on: https://code.engineering.redhat.com/gerrit/128478
Tested-by: Poornima Gurusiddaiah <pgurusid@redhat.com>
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 tests/bugs/md-cache/bug-1476324.t           | 27 +++++++++++++++++++++++++++
 xlators/performance/md-cache/src/md-cache.c |  2 +-
 2 files changed, 28 insertions(+), 1 deletion(-)
 create mode 100644 tests/bugs/md-cache/bug-1476324.t

diff --git a/tests/bugs/md-cache/bug-1476324.t b/tests/bugs/md-cache/bug-1476324.t
new file mode 100644
index 0000000..c34f412
--- /dev/null
+++ b/tests/bugs/md-cache/bug-1476324.t
@@ -0,0 +1,27 @@
+#!/bin/bash
+
+. $(dirname $0)/../../include.rc
+. $(dirname $0)/../../volume.rc
+
+cleanup;
+
+TEST glusterd;
+
+TEST $CLI volume create $V0 $H0:$B0/${V0}{1,2,3};
+
+TEST $CLI volume start $V0
+
+TEST $CLI volume set $V0 performance.md-cache-timeout 600
+TEST $CLI volume set $V0 performance.cache-samba-metadata on
+
+TEST glusterfs --volfile-id=/$V0 --volfile-server=$H0 $M0
+
+TEST touch $M0/file1
+
+TEST "setfattr -n user.DOSATTRIB -v 0sAAOW $M0/file1"
+TEST "getfattr -n user.DOSATTRIB $M0/file1 -e base64 | grep -q 0sAAOW"
+
+TEST "setfattr -n user.DOSATTRIB -v 0x00ff $M0/file1"
+TEST "getfattr -n user.DOSATTRIB $M0/file1 -e hex | grep -q 0x00ff"
+
+cleanup;
diff --git a/xlators/performance/md-cache/src/md-cache.c b/xlators/performance/md-cache/src/md-cache.c
index d21a6a7..1ca7727 100644
--- a/xlators/performance/md-cache/src/md-cache.c
+++ b/xlators/performance/md-cache/src/md-cache.c
@@ -633,7 +633,7 @@ updatefn(dict_t *dict, char *key, data_t *value, void *data)
                  * not update their cache if the value of a xattr is a 0 byte
                  * data (i.e. "").
                  */
-                if (!strcmp (value->data, ""))
+                if (value->len == 1 && value->data[0] == '\0')
                         continue;
 
 		if (dict_set(u->dict, key, value) < 0) {
-- 
1.8.3.1

