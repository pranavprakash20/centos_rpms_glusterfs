From c1d83132c4b2d49eb922fb7fe42952856aeff83d Mon Sep 17 00:00:00 2001
From: Raghavendra G <rgowdapp@redhat.com>
Date: Sat, 8 Sep 2018 19:53:07 +0530
Subject: [PATCH 371/385] performance/write-behind: remove the request from wip
 queue in wb_fulfill_request

The bug is very similar to bz 1379655 and the fix too very similar to
commit a8b2a981881221925bb5edfe7bb65b25ad855c04.

Before this patch, a request is removed from wip queue only when ref
count of request hits 0. Though, wb_fulfill_request does an unref,
it need not be the last unref and hence the request may survive in
wip queue till the last unref. Let,

T1: the time at which wb_fulfill_request is invoked
T2: the time at which last unref is done on request

Let's consider a case of T2 > T1. In the time window between T1 and
T2, any other request (waiter) conflicting with request in liability
queue (blocker - basically a write which has been lied) is blocked
from winding. If T2 happens to be when wb_do_unwinds is invoked, no
further processing of request list happens and "waiter" would get
blocked forever. An example imaginary sequence of events is given
below:

1. A write request w1 is picked up for winding in __wb_pick_winds
   and w1 is moved to wip queue. Let's call this
   invocation of wb_process_queue by wb_writev as PQ1. Note w1 is not
   unwound.

2. A dependent write (w2) hits write-behind and is unwound followed by
   a flush (f1) request. Since the liability queue
   of inode is not empty, w2 and f1 are not picked for unwinding. Let's call
   the invocation of wb_process_queue by wb_flush as PQ2. Note that
   invocation of wb_process_queue by w2 doesn't wind w2 instead
   unwinds it after which we hit PQ2

3. PQ2 continues and picks w1 for fulfilling and invokes
   wb_fulfill. As part of successful wb_fulfill_cbk,
   wb_fulfill_request (w1) is invoked. But, w1 is not freed (and hence
   not removed from wip queue) as w1 is not unwound _yet_ and a
   ref remains (PQ1 has not invoked wb_do_unwinds _yet_).

4. wb_fulfill_cbk (triggered by PQ2) invokes a wb_process_queue (let's
   say PQ3). w2 is not picked up for winding in PQ3 as w1 is still in wip
   queue. At this time, PQ2 and PQ3 are complete.

5. PQ1 continues, unwinds w1 and does last unref on w1 and w1 is freed
   (and removed from wip queue). Since PQ1 didn't invoke
   wb_fulfill on any other write requests, there won't be any future
   codepaths that would invoke wb_process_queue and w2 is stuck
   forever. This will prevent f2 too and hence close syscall is hung

With this fix, w1 is removed from liability queue in step 3 above and
PQ3 winds w2 in step 4 (as there are no requests conflicting with w2
in liability queue during execution of PQ3). Once w2 is complete, f1
is resumed.

>Change-Id: Ia972fad0858dc4abccdc1227cb4d880f85b3b89b
>Signed-off-by: Raghavendra G <rgowdapp@redhat.com>
>Fixes: bz#1626787

Change-Id: Ia972fad0858dc4abccdc1227cb4d880f85b3b89b
Signed-off-by: Raghavendra G <rgowdapp@redhat.com>
BUG: 1626780
upstream patch: https://review.gluster.org/21123
Reviewed-on: https://code.engineering.redhat.com/gerrit/149775
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 xlators/performance/write-behind/src/write-behind.c | 1 +
 1 file changed, 1 insertion(+)

diff --git a/xlators/performance/write-behind/src/write-behind.c b/xlators/performance/write-behind/src/write-behind.c
index ca1cb63..478985a 100644
--- a/xlators/performance/write-behind/src/write-behind.c
+++ b/xlators/performance/write-behind/src/write-behind.c
@@ -725,6 +725,7 @@ __wb_fulfill_request (wb_request_t *req)
 		*/
 	}
 
+        list_del_init (&req->wip);
 	__wb_request_unref (req);
 }
 
-- 
1.8.3.1

