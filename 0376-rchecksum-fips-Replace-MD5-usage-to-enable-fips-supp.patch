From f0e9776dd915c70bd9acb4e9624e8e2fd91ae7b7 Mon Sep 17 00:00:00 2001
From: Kotresh HR <khiremat@redhat.com>
Date: Tue, 19 Dec 2017 07:21:07 -0500
Subject: [PATCH 376/385] rchecksum/fips: Replace MD5 usage to enable fips
 support

rchecksum uses MD5 which is not fips compliant. Hence
using sha256 for the same.

Backport of:
 > Patch: https://review.gluster.org/19052
 > Updates: #230
 > Change-Id: I7fad016fcc2a9900395d0da919cf5ba996ec5278
 > Signed-off-by: Kotresh HR <khiremat@redhat.com>

BUG: 1459709
Change-Id: I7fad016fcc2a9900395d0da919cf5ba996ec5278
Signed-off-by: Kotresh HR <khiremat@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/149771
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 libglusterfs/src/checksum.c                    | 8 +++++---
 libglusterfs/src/default-args.c                | 2 +-
 xlators/cluster/afr/src/afr-self-heal-common.c | 2 +-
 xlators/cluster/afr/src/afr-self-heal-data.c   | 4 ++--
 xlators/cluster/afr/src/afr.h                  | 2 +-
 xlators/mgmt/glusterd/src/glusterd-utils.c     | 2 +-
 xlators/protocol/server/src/server-common.c    | 2 +-
 xlators/storage/bd/src/bd.c                    | 4 +---
 xlators/storage/posix/src/posix.c              | 3 +--
 9 files changed, 14 insertions(+), 15 deletions(-)

diff --git a/libglusterfs/src/checksum.c b/libglusterfs/src/checksum.c
index 5fac133..a7f9877 100644
--- a/libglusterfs/src/checksum.c
+++ b/libglusterfs/src/checksum.c
@@ -8,9 +8,10 @@
   cases as published by the Free Software Foundation.
 */
 
-#include <openssl/md5.h>
+#include <openssl/sha.h>
 #include <zlib.h>
 #include <stdint.h>
+#include <string.h>
 
 /*
  * The "weak" checksum required for the rsync algorithm.
@@ -30,7 +31,8 @@ gf_rsync_weak_checksum (unsigned char *buf, size_t len)
  * The "strong" checksum required for the rsync algorithm.
  */
 void
-gf_rsync_strong_checksum (unsigned char *data, size_t len, unsigned char *md5)
+gf_rsync_strong_checksum (unsigned char *data, size_t len,
+                          unsigned char *sha256_md)
 {
-        MD5 (data, len, md5);
+        SHA256((const unsigned char *)data, len, sha256_md);
 }
diff --git a/libglusterfs/src/default-args.c b/libglusterfs/src/default-args.c
index f40de2d..3ccf52a 100644
--- a/libglusterfs/src/default-args.c
+++ b/libglusterfs/src/default-args.c
@@ -1140,7 +1140,7 @@ args_rchecksum_cbk_store (default_args_cbk_t *args,
                 args->weak_checksum =
                         weak_checksum;
                 args->strong_checksum =
-                        memdup (strong_checksum, MD5_DIGEST_LENGTH);
+                        memdup (strong_checksum, SHA256_DIGEST_LENGTH);
         }
         if (xdata)
                 args->xdata = dict_ref (xdata);
diff --git a/xlators/cluster/afr/src/afr-self-heal-common.c b/xlators/cluster/afr/src/afr-self-heal-common.c
index 50989d6..2989b9e 100644
--- a/xlators/cluster/afr/src/afr-self-heal-common.c
+++ b/xlators/cluster/afr/src/afr-self-heal-common.c
@@ -665,7 +665,7 @@ afr_reply_copy (struct afr_reply *dst, struct afr_reply *src)
 	if (dst->xdata)
 		dict_unref (dst->xdata);
 	dst->xdata = xdata;
-	memcpy (dst->checksum, src->checksum, MD5_DIGEST_LENGTH);
+	memcpy (dst->checksum, src->checksum, SHA256_DIGEST_LENGTH);
 }
 
 void
diff --git a/xlators/cluster/afr/src/afr-self-heal-data.c b/xlators/cluster/afr/src/afr-self-heal-data.c
index 3ef7376..dd44deb 100644
--- a/xlators/cluster/afr/src/afr-self-heal-data.c
+++ b/xlators/cluster/afr/src/afr-self-heal-data.c
@@ -42,7 +42,7 @@ __checksum_cbk (call_frame_t *frame, void *cookie, xlator_t *this,
                 replies[i].buf_has_zeroes = dict_get_str_boolean (xdata,
                                                    "buf-has-zeroes", _gf_false);
 	if (strong)
-		memcpy (local->replies[i].checksum, strong, MD5_DIGEST_LENGTH);
+            memcpy (local->replies[i].checksum, strong, SHA256_DIGEST_LENGTH);
 
 	syncbarrier_wake (&local->barrier);
 	return 0;
@@ -92,7 +92,7 @@ __afr_can_skip_data_block_heal (call_frame_t *frame, xlator_t *this, fd_t *fd,
                 if (local->replies[i].valid) {
                         if (memcmp (local->replies[source].checksum,
                                     local->replies[i].checksum,
-                                    MD5_DIGEST_LENGTH)) {
+                                    SHA256_DIGEST_LENGTH)) {
                                 checksum_match = _gf_false;
                                 break;
                         }
diff --git a/xlators/cluster/afr/src/afr.h b/xlators/cluster/afr/src/afr.h
index 35928a9..7cb6f00 100644
--- a/xlators/cluster/afr/src/afr.h
+++ b/xlators/cluster/afr/src/afr.h
@@ -271,7 +271,7 @@ struct afr_reply {
 	struct iatt preparent2;
 	struct iatt postparent2;
         /* For rchecksum */
-	uint8_t checksum[MD5_DIGEST_LENGTH];
+	uint8_t checksum[SHA256_DIGEST_LENGTH];
         gf_boolean_t buf_has_zeroes;
         /* For lookup */
         int8_t need_heal;
diff --git a/xlators/mgmt/glusterd/src/glusterd-utils.c b/xlators/mgmt/glusterd/src/glusterd-utils.c
index 3db3a15..2a176be 100644
--- a/xlators/mgmt/glusterd/src/glusterd-utils.c
+++ b/xlators/mgmt/glusterd/src/glusterd-utils.c
@@ -1873,7 +1873,7 @@ glusterd_set_brick_socket_filepath (glusterd_volinfo_t *volinfo,
         char                    sock_filepath[PATH_MAX] = {0,};
 
         expected_file_len = strlen (GLUSTERD_SOCK_DIR) + strlen ("/") +
-                            MD5_DIGEST_LENGTH*2 + strlen (".socket") + 1;
+                            SHA256_DIGEST_LENGTH*2 + strlen (".socket") + 1;
         GF_ASSERT (len >= expected_file_len);
         this = THIS;
         GF_ASSERT (this);
diff --git a/xlators/protocol/server/src/server-common.c b/xlators/protocol/server/src/server-common.c
index ce33089..9c38706 100644
--- a/xlators/protocol/server/src/server-common.c
+++ b/xlators/protocol/server/src/server-common.c
@@ -298,7 +298,7 @@ server_post_rchecksum (gfs3_rchecksum_rsp *rsp, uint32_t weak_checksum,
         rsp->weak_checksum = weak_checksum;
 
         rsp->strong_checksum.strong_checksum_val = (char *)strong_checksum;
-        rsp->strong_checksum.strong_checksum_len = MD5_DIGEST_LENGTH;
+        rsp->strong_checksum.strong_checksum_len = SHA256_DIGEST_LENGTH;
 
 }
 
diff --git a/xlators/storage/bd/src/bd.c b/xlators/storage/bd/src/bd.c
index af3ac84..64b34d9 100644
--- a/xlators/storage/bd/src/bd.c
+++ b/xlators/storage/bd/src/bd.c
@@ -2148,7 +2148,7 @@ bd_rchecksum (call_frame_t *frame, xlator_t *this, fd_t *fd, off_t offset,
         char           *buf             = NULL;
         int32_t         weak_checksum   = 0;
         bd_fd_t        *bd_fd           = NULL;
-        unsigned char   strong_checksum[MD5_DIGEST_LENGTH] = {0};
+        unsigned char   strong_checksum[SHA256_DIGEST_LENGTH] = {0};
 
         VALIDATE_OR_GOTO (frame, out);
         VALIDATE_OR_GOTO (this, out);
@@ -2162,8 +2162,6 @@ bd_rchecksum (call_frame_t *frame, xlator_t *this, fd_t *fd, off_t offset,
                 return 0;
         }
 
-        memset (strong_checksum, 0, MD5_DIGEST_LENGTH);
-
         alloc_buf = page_aligned_alloc (len, &buf);
         if (!alloc_buf) {
                 op_errno = ENOMEM;
diff --git a/xlators/storage/posix/src/posix.c b/xlators/storage/posix/src/posix.c
index efbf804..4e13465 100644
--- a/xlators/storage/posix/src/posix.c
+++ b/xlators/storage/posix/src/posix.c
@@ -7000,7 +7000,7 @@ posix_rchecksum (call_frame_t *frame, xlator_t *this,
         ssize_t                 bytes_read      = 0;
         int32_t                 weak_checksum   = 0;
         int32_t                 zerofillcheck   = 0;
-        unsigned char           strong_checksum[MD5_DIGEST_LENGTH] = {0};
+        unsigned char           strong_checksum[SHA256_DIGEST_LENGTH] = {0};
         struct posix_private    *priv           = NULL;
         dict_t                  *rsp_xdata      = NULL;
         gf_boolean_t            buf_has_zeroes  = _gf_false;
@@ -7010,7 +7010,6 @@ posix_rchecksum (call_frame_t *frame, xlator_t *this,
         VALIDATE_OR_GOTO (fd, out);
 
         priv = this->private;
-        memset (strong_checksum, 0, MD5_DIGEST_LENGTH);
 
         alloc_buf = _page_aligned_alloc (len, &buf);
         if (!alloc_buf) {
-- 
1.8.3.1

