From d5c5cbe82ef0f7bf8686e71cf08b92e7baf62f55 Mon Sep 17 00:00:00 2001
From: Ravishankar N <ravishankar@redhat.com>
Date: Sat, 6 Oct 2018 00:50:53 +0530
Subject: [PATCH 395/399] index: prevent arbitrary file creation outside
 entry-changes folder

Problem:
A compromised client can set arbitrary values for the GF_XATTROP_ENTRY_IN_KEY
and GF_XATTROP_ENTRY_OUT_KEY during xattrop fop. These values are
consumed by index as a filename to be created/deleted according to the key.
Thus it is possible to create/delete random files even outside the gluster
volume boundary.

Fix:
Index expects the filename to be a basename, i.e. it must not contain any
pathname components like "/" or "../". Enforce this.

Fixes: CVE-2018-14654
BUG: 1634671
Change-Id: Ib01c35414c36e3101af9e99a1ea17535ef8bd3b3
Signed-off-by: Ravishankar N <ravishankar@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/151985
Reviewed-by: Amar Tumballi <amarts@redhat.com>
Reviewed-by: Sunil Kumar Heggodu Gopala Acharya <sheggodu@redhat.com>
---
 xlators/features/index/src/index.c | 19 +++++++++++++++++++
 1 file changed, 19 insertions(+)

diff --git a/xlators/features/index/src/index.c b/xlators/features/index/src/index.c
index bf3f4dd..89cdbda 100644
--- a/xlators/features/index/src/index.c
+++ b/xlators/features/index/src/index.c
@@ -852,6 +852,15 @@ index_entry_create (xlator_t *this, inode_t *inode, char *filename)
                 ctx->state[ENTRY_CHANGES] = IN;
         }
 
+        if  (strchr (filename, '/')) {
+                gf_msg (this->name, GF_LOG_ERROR, EINVAL,
+                        INDEX_MSG_INDEX_ADD_FAILED,
+                        "Got invalid entry (%s) for pargfid path (%s)",
+                        filename, pgfid_path);
+                op_errno = EINVAL;
+                goto out;
+        }
+
         op_errno = 0;
 
         snprintf (entry_path, sizeof(entry_path), "%s/%s", pgfid_path,
@@ -886,6 +895,16 @@ index_entry_delete (xlator_t *this, uuid_t pgfid, char *filename)
 
         make_gfid_path (priv->index_basepath, ENTRY_CHANGES_SUBDIR, pgfid,
                         pgfid_path, sizeof (pgfid_path));
+
+        if  (strchr (filename, '/')) {
+                gf_msg (this->name, GF_LOG_ERROR, EINVAL,
+                        INDEX_MSG_INDEX_DEL_FAILED,
+                        "Got invalid entry (%s) for pargfid path (%s)",
+                        filename, pgfid_path);
+                op_errno = EINVAL;
+                goto out;
+        }
+
         snprintf (entry_path, sizeof(entry_path), "%s/%s", pgfid_path,
                   filename);
 
-- 
1.8.3.1

