From 1a24a7942fe9ecccaf29ae9bc125cd9b08fc8906 Mon Sep 17 00:00:00 2001
From: karthik-us <ksubrahm@redhat.com>
Date: Mon, 5 Nov 2018 17:57:55 +0530
Subject: [PATCH 417/444] cli: Add warning message while converting to replica
 2 configuration

Backport of: https://review.gluster.org/#/c/glusterfs/+/21136/

Currently while creating replica 2 volume we display a warning message
of ending up in split-brain. But while converting an existing volume
from other configuration to replica 2 by add-brick or remove-brick
operations we do not show any such messages.
With this fix in add-brick and remove-brick cases also we will display
the same warning message and prompt for confirmation if the configuration
changes to replica 2.

Change-Id: Id7b1a40e80fca3e1043b802fa5f7c3b656ef2228
BUG: 1579758
Signed-off-by: karthik-us <ksubrahm@redhat.com>
Reviewed-on: https://code.engineering.redhat.com/gerrit/154947
Tested-by: RHGS Build Bot <nigelb@redhat.com>
Reviewed-by: Ravishankar Narayanankutty <ravishankar@redhat.com>
Reviewed-by: Atin Mukherjee <amukherj@redhat.com>
---
 cli/src/cli-cmd-parser.c | 48 +++++++++++++++++++++++++++++++++++++++++++-----
 cli/src/cli-cmd-volume.c | 11 +++++++----
 cli/src/cli.h            | 10 +++++-----
 3 files changed, 55 insertions(+), 14 deletions(-)

diff --git a/cli/src/cli-cmd-parser.c b/cli/src/cli-cmd-parser.c
index 7917d66..3745fb4 100644
--- a/cli/src/cli-cmd-parser.c
+++ b/cli/src/cli-cmd-parser.c
@@ -1774,8 +1774,8 @@ out:
 }
 
 int32_t
-cli_cmd_volume_add_brick_parse (const char **words, int wordcount,
-                                dict_t **options, int *ret_type)
+cli_cmd_volume_add_brick_parse (struct cli_state *state, const char **words,
+                                int wordcount, dict_t **options, int *ret_type)
 {
         dict_t  *dict = NULL;
         char    *volname = NULL;
@@ -1790,6 +1790,8 @@ cli_cmd_volume_add_brick_parse (const char **words, int wordcount,
         int     index;
         gf_boolean_t is_force = _gf_false;
         int wc = wordcount;
+        gf_answer_t answer = GF_ANSWER_NO;
+        const char *question = NULL;
 
         GF_ASSERT (words);
         GF_ASSERT (options);
@@ -1854,6 +1856,23 @@ cli_cmd_volume_add_brick_parse (const char **words, int wordcount,
                                 goto out;
                         index = 7;
                 }
+
+                if (count == 2) {
+                        if (strcmp (words[wordcount - 1], "force")) {
+                                question = "Replica 2 volumes are prone to "
+                                           "split-brain. Use Arbiter or "
+                                           "Replica 3 to avoid this.\n"
+                                           "Do you still want to continue?\n";
+                                answer = cli_cmd_get_confirmation (state,
+                                                                   question);
+                                if (GF_ANSWER_NO == answer) {
+                                        gf_log ("cli", GF_LOG_ERROR, "Add brick"
+                                                " cancelled, exiting");
+                                        ret = -1;
+                                        goto out;
+                                }
+                        }
+                }
         } else if ((strcmp (w, "stripe")) == 0) {
                 type = GF_CLUSTER_TYPE_STRIPE;
                 count = strtol (words[4], NULL, 0);
@@ -2061,9 +2080,9 @@ out:
 }
 
 int32_t
-cli_cmd_volume_remove_brick_parse (const char **words, int wordcount,
-                                   dict_t **options, int *question,
-                                   int *brick_count)
+cli_cmd_volume_remove_brick_parse (struct cli_state *state, const char **words,
+                                   int wordcount, dict_t **options,
+                                   int *question, int *brick_count)
 {
         dict_t  *dict = NULL;
         char    *volname = NULL;
@@ -2081,6 +2100,8 @@ cli_cmd_volume_remove_brick_parse (const char **words, int wordcount,
         char    *w = NULL;
         int32_t  command = GF_OP_CMD_NONE;
         long     count = 0;
+        gf_answer_t answer = GF_ANSWER_NO;
+        const char *ques = NULL;
 
         GF_ASSERT (words);
         GF_ASSERT (options);
@@ -2115,6 +2136,23 @@ cli_cmd_volume_remove_brick_parse (const char **words, int wordcount,
                         goto out;
                 }
 
+                if (count == 2) {
+                        if (strcmp (words[wordcount - 1], "force")) {
+                                ques = "Replica 2 volumes are prone to "
+                                       "split-brain. Use Arbiter or Replica 3 "
+                                       "to avoid this.\n"
+                                       "Do you still want to continue?\n";
+                                answer = cli_cmd_get_confirmation (state,
+                                                                   ques);
+                                if (GF_ANSWER_NO == answer) {
+                                        gf_log ("cli", GF_LOG_ERROR, "Remove "
+                                                "brick cancelled, exiting");
+                                        ret = -1;
+                                        goto out;
+                                }
+                        }
+                }
+
                 ret = dict_set_int32 (dict, "replica-count", count);
                 if (ret)
                         goto out;
diff --git a/cli/src/cli-cmd-volume.c b/cli/src/cli-cmd-volume.c
index a1f0840..32efa73 100644
--- a/cli/src/cli-cmd-volume.c
+++ b/cli/src/cli-cmd-volume.c
@@ -1021,7 +1021,8 @@ cli_cmd_volume_add_brick_cbk (struct cli_state *state,
         if (!frame)
                 goto out;
 
-        ret = cli_cmd_volume_add_brick_parse (words, wordcount, &options, 0);
+        ret = cli_cmd_volume_add_brick_parse (state, words, wordcount, &options,
+                                              0);
         if (ret) {
                 cli_usage_out (word->pattern);
                 parse_error = 1;
@@ -1151,7 +1152,8 @@ do_cli_cmd_volume_attach_tier (struct cli_state *state,
         if (!frame)
                 goto out;
 
-        ret = cli_cmd_volume_add_brick_parse (words, wordcount, &options, &type);
+        ret = cli_cmd_volume_add_brick_parse (state, words, wordcount, &options,
+                                              &type);
         if (ret) {
                 cli_usage_out (word->pattern);
                 parse_error = 1;
@@ -2032,8 +2034,9 @@ cli_cmd_volume_remove_brick_cbk (struct cli_state *state,
         if (!frame)
                 goto out;
 
-        ret = cli_cmd_volume_remove_brick_parse (words, wordcount, &options,
-                                                 &need_question, &brick_count);
+        ret = cli_cmd_volume_remove_brick_parse (state, words, wordcount,
+                                                 &options, &need_question,
+                                                 &brick_count);
         if (ret) {
                 cli_usage_out (word->pattern);
                 parse_error = 1;
diff --git a/cli/src/cli.h b/cli/src/cli.h
index c9bf93d..109dcd4 100644
--- a/cli/src/cli.h
+++ b/cli/src/cli.h
@@ -264,8 +264,8 @@ cli_cmd_get_state_parse (struct cli_state *state, const char **words,
                          int wordcount, dict_t **options, char **op_errstr);
 
 int32_t
-cli_cmd_volume_add_brick_parse (const char **words, int wordcount,
-                                dict_t **options, int *type);
+cli_cmd_volume_add_brick_parse (struct cli_state *state, const char **words,
+                                int wordcount, dict_t **options, int *type);
 
 int32_t
 cli_cmd_volume_detach_tier_parse (const char **words, int wordcount,
@@ -280,9 +280,9 @@ cli_cmd_volume_old_tier_parse (const char **words, int wordcount,
                            dict_t **options);
 
 int32_t
-cli_cmd_volume_remove_brick_parse (const char **words, int wordcount,
-                                   dict_t **options, int *question,
-                                   int *brick_count);
+cli_cmd_volume_remove_brick_parse (struct cli_state *state, const char **words,
+                                   int wordcount, dict_t **options,
+                                   int *question, int *brick_count);
 
 int32_t
 cli_cmd_volume_replace_brick_parse (const char **words, int wordcount,
-- 
1.8.3.1

